/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  async redirects() {
    return [
      {
      source: '/',
      destination: '/top-stories',
      permanent: true
      }
    ]
  },
  env: {
    BACKEND_BASE_URI: process.env.BACKEND_BASE_URI
  }
}

module.exports = nextConfig
