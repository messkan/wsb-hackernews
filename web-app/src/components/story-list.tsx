import { Story as StoryType } from '@/types/story';
import { Story } from '@/components/story';
import { useStories } from '@/hooks/use-stories';
import { Spinner } from '@/components/spinner';

export function StoryList({
  initialStories,
  category,
}: {
  initialStories: StoryType[];
  category: string;
}) {
  const [stories, loadMoreStories, isLoading] = useStories({
    initialStories,
    category: category,
  });

  return (
    <>
      {stories.map((story: StoryType) => (
        <Story story={story} key={story.id} />
      ))}
      <div className='mt-2 flex flex-col items-center justify-center'>
        {isLoading ? (
          <Spinner></Spinner>
        ) : (
          <button
            className='mt-8 rounded-full 
            bg-gradient-to-r 
            from-blue-500 
            to-purple-500 
            py-2 px-4 
            font-semibold 
            text-white 
            shadow-md 
            hover:bg-opacity-75'
            onClick={loadMoreStories}
          >
            Load more
          </button>
        )}
      </div>
    </>
  );
}
