import { Navbar } from '@/components/navbar';
import { useState, useEffect } from 'react';
import { Spinner } from '@/components/spinner';
import Router from 'next/router';

export function Layout({ children }) {
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    Router.events.on('routeChangeStart', () => setLoading(true));
    Router.events.on('routeChangeComplete', () => setLoading(false));
    Router.events.on('routeChangeError', () => setLoading(false));
    return () => {
      Router.events.off('routeChangeStart', () => setLoading(true));
      Router.events.off('routeChangeComplete', () => setLoading(false));
      Router.events.off('routeChangeError', () => setLoading(false));
    };
  }, []);

  return (
    <div>
      <Navbar></Navbar>
      {loading ? (
        <div className='mt-4'>
          <Spinner></Spinner>
        </div>
      ) : (
        children
      )}
    </div>
  );
}
