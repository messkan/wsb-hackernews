import { Story as StoryType } from '@/types/story';

export function Story({ story }: { story: StoryType }) {
  return (
    <div className='mt-2 rounded-md bg-white p-4 shadow-md'>
      <div className='mb-2 flex items-center'>
        <div className='mr-2'>
          <span className='text-gray-500'>{story.id}</span>
        </div>
        <div>
          <a
            target='_blank'
            href={story.url ?? '#'}
            className='font-medium text-black hover:underline'
            rel='noreferrer'
          >
            {story.title}
          </a>
          <span className='text-sm text-gray-400'> ({story.url})</span>
        </div>
      </div>
      <div className='flex items-center text-sm text-gray-500'>
        <div className='mr-2'>
          <svg
            xmlns='http://www.w3.org/2000/svg'
            className='inline-block h-4 w-4'
            fill='none'
            viewBox='0 0 24 24'
            stroke='currentColor'
          >
            <path
              strokeLinecap='round'
              strokeLinejoin='round'
              strokeWidth='2'
              d='M9 5l7 7-7 7'
            />
          </svg>
          <span className='ml-1'>{story.score} points</span>
        </div>
        <div className='mr-2'>
          <svg
            xmlns='http://www.w3.org/2000/svg'
            className='inline-block h-4 w-4'
            fill='none'
            viewBox='0 0 24 24'
            stroke='currentColor'
          >
            <path
              strokeLinecap='round'
              strokeLinejoin='round'
              strokeWidth='2'
              d='M19 14v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5m16-9l-1.58 1.58a2 2 0 01-2.83 0L13 5.83l-1.58 1.58a2 2 0 01-2.83 0L5 5'
            />
          </svg>
          <span className='ml-1'>{story.descendants} comments</span>
        </div>
        <div>
          <span className='ml-1'>
            by{' '}
            <a href='#' className='font-medium text-gray-900 hover:underline'>
              {story.by}
            </a>
          </span>
        </div>
      </div>
    </div>
  );
}
