import Link from 'next/link';
import { useRouter } from 'next/router';

export function Navbar() {
  const router = useRouter();

  return (
    <nav className='bg-orange-600'>
      <div className='mx-auto px-4 sm:px-6 lg:px-8'>
        <div className='flex h-16 items-center justify-between'>
          <div className='flex items-center'>
            <Link href='' className='text-2xl font-bold text-white'>
              Hacker News
            </Link>
          </div>
          <div className='hidden md:block'>
            <div className='ml-10 flex items-center space-x-4'>
              <Link
                href='top-stories'
                className={`text-lg font-medium text-gray-300 hover:text-white ${
                  router.pathname == '/top-stories' ? 'text-indigo-500' : ''
                }`}
              >
                Top Stories
              </Link>
              <Link
                href='new-stories'
                className={`text-lg font-medium text-gray-300 hover:text-white ${
                  router.pathname == '/new-stories' ? 'text-indigo-500' : ''
                }`}
              >
                New Stories
              </Link>
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
}
