import React from 'react';

export class ErrorBoundary extends React.Component<
  {
    children?: React.ReactNode;
  },
  { hasError: boolean; errorInfo?: React.ErrorInfo; error?: Error }
> {
  state = { hasError: false, error: undefined, errorInfo: undefined };

  static getDerivedStateFromError(error: Error) {
    return { hasError: true, error };
  }

  componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {
    this.setState((prev) => ({
      ...prev,
      error,
      errorInfo,
    }));
  }

  render() {
    const error = this.state.error as Error | undefined;
    if (this.state.hasError && error) {
      return <>Something went wrong</>;
    }

    return this.props.children;
  }
}
