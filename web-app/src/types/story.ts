export type Story = {
  id: number;
  by: string | null;
  kids: null | number[];
  descendants: null | number;
  title: null | string;
  url: null | string;
  score: number;
};
