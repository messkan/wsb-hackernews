import { Story } from '@/types/story';

const BASE_URI = process.env.BACKEND_BASE_URI || 'http://localhost:8000';

export async function fetchStories(
  category: string,
  pageNumber = 0,
  limit = 5
): Promise<Array<Story>> {
  const url = `${BASE_URI}/api/stories/${category}?page=${pageNumber}&limit=${limit}`;
  const response = await fetch(url);

  if (response.ok === false) {
    throw new Error('Something went wrong');
  }

  const json: number[] = await response.json();
  const promises = json.map((id: number) =>
    fetch(`${BASE_URI}/api/stories/${id}`).then((response) => response.json())
  );
  return await Promise.all(promises);
}
