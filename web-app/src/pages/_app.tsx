import { ErrorBoundary } from '@/components/error-boundary';
import { Layout } from '@/components/layout';
import { Spinner } from '@/components/spinner';
import '@/styles/globals.css';
import type { AppProps } from 'next/app';
import { Suspense } from 'react';

export default function App({ Component, pageProps }: AppProps) {
  return (
    <ErrorBoundary>
      <Layout>
        <Suspense fallback={<Spinner />}>
          <Component {...pageProps} />
        </Suspense>
      </Layout>
    </ErrorBoundary>
  );
}
