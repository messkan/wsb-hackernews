import { fetchStories } from '@/api/stories';
import { StoryList } from '@/components/story-list';
import { Story } from '@/types/story';

export async function getServerSideProps() {
  const stories = await fetchStories('topstories');
  return {
    props: { stories },
  };
}

export default function TopStories({ stories }: { stories: Story[] }) {
  return <StoryList initialStories={stories} category='topstories'></StoryList>;
}
