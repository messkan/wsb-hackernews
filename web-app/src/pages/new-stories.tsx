import { fetchStories } from '@/api/stories';
import { StoryList } from '@/components/story-list';
import { Story } from '@/types/story';

export async function getServerSideProps() {
  const stories = await fetchStories('newstories');
  return {
    props: { stories },
  };
}

export default function NewStories({ stories }: { stories: Story[] }) {
  return <StoryList initialStories={stories} category='newstories'></StoryList>;
}
