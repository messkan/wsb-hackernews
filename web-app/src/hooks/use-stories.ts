import { useEffect, useState } from 'react';
import { fetchStories } from '@/api/stories';
import { Story } from '@/types/story';

export function useStories({
  initialStories,
  category,
}: {
  initialStories: Story[];
  category: string;
}) {
  const [stories, setStories] = useState(initialStories);
  const [pageNumber, setPageNumber] = useState(0);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    async function loadMore() {
      setIsLoading(true);
      const newStories = await fetchStories(category, pageNumber);
      setIsLoading(false);
      setStories((stories) => [...stories, ...newStories]);
    }
    if (pageNumber !== 0) {
      loadMore()
        .then(() => {})
        .catch(() => {});
    }
  }, [pageNumber, category]);

  function loadMoreStories() {
    setPageNumber((pageNumber) => pageNumber + 1);
  }

  return [stories, loadMoreStories, isLoading];
}
