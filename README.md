# WSB Hacker news api
This code is just an implementation for a coding challenge. It's a clone of hacker news.
This projects contains two application, backend app using Symfony and web app using Next js.

# Table of Contents
  - <b> Symfony Hacker News API
  - <b> Installation of Symfony app
  - <b> Testing
  - <b> Next.js Hacker News App
  - <b> Installation of next app
  - <b> Documentation
  - <b> Pipeline Stages
  - <b> Contributing
  - <b> Preview
  - <b> License



## Symfony Hacker News API
This project is a RESTful API built with Symfony 6.2 that provides access to the Hacker News API. 
The API can retrieve and paginate stories by category, and retrieve individual stories by their ID. The API has been documented using OpenAPI (formerly known as Swagger), and the documentation is available `localhost:8000/doc` , or follow this link.


# Installation of Symfony app
To install the Symfony Hacker News API, follow these steps:

 - Clone the repository and navigate to the root directory.
 - Run composer install to install the PHP dependencies.
 - Create a .env file in the root directory and set the HACKER_NEWS_API_BASE_URL environment variable to the base URL of the Hacker News API.
 - Run symfony serve to start the development server.
 - The API should now be available at http://localhost:8000.

# Testing
To run the PHPUnit tests, run php bin/phpunit. Actually, there's unit tests for StoryResolver class



# Next.js Hacker News App
This project is a web application built with Next.js that consumes the Symfony Hacker News API. The application allows users to view and paginate stories by category, and view stories.

# Installation of Next js app
To install the Next.js Hacker News App, follow these steps:

 - Clone the repository and navigate to the root directory.
 - Run pnpm install to install the Node.js dependencies.
  - Create a .env.local file in the root directory and set the Next.js Hacker News App environment variable to the URL of the Symfony Hacker News API.
 - Run npm run dev to start the development server.
 - The application should now be available at http://localhost:3000.

# Documentation

There's documentation using OpenAPI, and redoc, available at http://localhost:8000/doc.

This are the docs of the two endpoints: 

<a href="https://ibb.co/xmTnX4Z"><img src="https://i.ibb.co/yhjzd32/image.png" alt="image" border="0" /></a>

<a href="https://ibb.co/2h3vRGw"><img src="https://i.ibb.co/7CtnBsH/image.png" alt="image" border="0" /></a>


# Pipeline Stages
The project's GitLab CI/CD pipeline has three stages:

 - <b>Lint </b>: This stage ensures that the frontend code is consistent and follows the code style guidelines. The lint stage uses ESLint to check the code quality of the frontend code. It is important to run the lint stage before committing the code to avoid code style violations.

  - <b> PHPCS </b>: 
The PHPCS stage checks the backend code for code style violations using PHP_CodeSniffer (PHPCS). This stage ensures that the PHP code follows the PSR-12 coding standard.

  - <b> Test </b>: 
This stage runs PHPUnit to execute the unit tests in the backend. The tests ensure that the code is working as expected and that any changes made to the code do not break the existing functionality.

# Contributing

Contributions are welcome and appreciated. To contribute to the project, please follow these steps:

- Clone the repository: git clone https://gitlab.com/messkan/wsb-hackernews.git
- Create a new branch: git checkout -b feature/your-feature-name
- Make your changes and test your code thoroughly.
Run the linter and PHPCS to ensure your code meets the coding guidelines.
- Run the unit tests to ensure your code is working as expected.
- Commit your changes: git commit -am 'Add some feature'
- Push to the branch: git push origin feature/your-feature-name
- Create a new merge request on GitLab.
- Wait for a code review and make any necessary changes.
- Once the merge request is approved, it will be merged into the main branch.


# Preview

 
 <a href="https://ibb.co/dtpDr4x"><img src="https://i.ibb.co/G9MR0QN/image.png" alt="image" border="0" /></a>

 <a href="https://ibb.co/sqFTTFf"><img src="https://i.ibb.co/QM6WW6g/image.png" alt="image" border="0" /></a>

 
# License
This project is licensed under the MIT License.
