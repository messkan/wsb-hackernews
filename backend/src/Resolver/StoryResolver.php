<?php

declare(strict_types=1);

namespace App\Resolver;

use App\Serializable\Story;
use App\Service\HackerNewsService;

class StoryResolver
{
    public function __construct(private HackerNewsService $hackerNewsService)
    {
    }

    /** @return int[] */
    public function paginateByCategory(string $category, int $limit, int $page): array
    {
        $stories = $this->hackerNewsService->getStoriesByCategory($category);

        return \array_slice($stories, $page * $limit, $limit);
    }

    public function get(int $id): ?Story
    {
        $story = $this->hackerNewsService->getStoryById($id);

        return $story;
    }
}
