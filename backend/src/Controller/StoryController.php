<?php

declare(strict_types=1);

namespace App\Controller;

use App\Exception\HackerNewsApiException;
use App\Resolver\StoryResolver;
use App\Serializable\Story;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/stories')]
class StoryController extends AbstractController
{
    public function __construct(private StoryResolver $resolver)
    {
    }

    #[Route(
        '/{category}',
        name: 'api_stories_paginate_by_category',
        methods: ['GET'],
        condition: 'params["category"] in [ "topstories", "newstories" ]'
    )
    ]
    #[OA\Response(
        response: 200,
        description: 'Returns an array of story IDs',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(type: 'integer'),
            example: [1, 2, 3]
        ),
    )]
    #[OA\Parameter(
        name: 'category',
        description: 'The category of the stories to fetch',
        in: 'path',
        required: true,
        schema: new OA\Schema(type: 'string', enum: ['topstories', 'newstories'])
    )]
    #[OA\Parameter(
        name: 'page',
        description: 'The page number of the pagination (default: 0)',
        in: 'query',
        required: false,
        schema: new OA\Schema(type: 'integer')
    )]
    #[OA\Parameter(
        name: 'page',
        description: 'The maximum number of ids to return (default: 10)',
        in: 'query',
        required: false,
        schema: new OA\Schema(type: 'integer')
    )]
    /** paginate stories */
    public function paginateByCategory(
        Request $request,
        string $category
    ): JsonResponse {
        try {
            $limit = (int) ($request->query->get('limit') ?? 10);
            $page = (int) $request->query->get('page');

            return $this->json(data: $this->resolver->paginateByCategory($category, $limit, $page));
        } catch (HackerNewsApiException $exception) {
            return $this->json(
                data: $exception->getMessage(),
                status: $exception->getStatusCode()
            );
        }
    }

    #[Route('/{id}', name: 'api_stories_get', requirements: ['id' => '\d+'], methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: 'Returns a story object if found',
        content: new OA\JsonContent(ref: new Model(type: Story::class))
    )]
    #[OA\Response(
        response: 404,
        description: 'When there is no story with the given id',
    )]
    #[OA\Parameter(
        name: 'id',
        description: 'The id of the story to fetch',
        in: 'path',
        required: true,
        schema: new OA\Schema(type: 'integer')
    )]
    /** Get by id */
    public function get(int $id): JsonResponse
    {
        try {
            $story = $this->resolver->get($id);
            if (!$story) {
                return $this->json(data: ['message' => 'Story not found'], status: 404);
            }

            return $this->json($story);
        } catch (HackerNewsApiException $exception) {
            return $this->json(
                data: $exception->getMessage(),
                status: $exception->getStatusCode()
            );
        }
    }
}
