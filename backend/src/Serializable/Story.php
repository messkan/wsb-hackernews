<?php

declare(strict_types=1);

namespace App\Serializable;

class Story
{
    private int $id;

    private ?string $by = null;

    /** @var int[] */
    private ?array $kids;

    private ?int $descendants = null;

    private ?int $score = null;

    private ?string $title = null;

    private ?string $url = null;

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setBy(?string $by): self
    {
        $this->by = $by;

        return $this;
    }

    public function getBy(): ?string
    {
        return $this->by;
    }

    /**
     * @param int[] $kids
     */
    public function setKids(?array $kids): self
    {
        $this->kids = $kids;

        return $this;
    }

    /**
     * @return int[]
     */
    public function getKids(): ?array
    {
        return $this->kids;
    }

    public function setDescendants(?int $descendants): self
    {
        $this->descendants = $descendants;

        return $this;
    }

    public function getDescendants(): ?int
    {
        return $this->descendants;
    }

    public function setScore(?int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }
}
