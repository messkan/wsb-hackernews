<?php

declare(strict_types=1);

namespace App\Service;

use App\Exception\HackerNewsApiException;
use App\Serializable\Story;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class HackerNewsService
{
    public function __construct(
        private SerializerInterface $serializer,
        private HttpClientInterface $hackerNewsClient
    ) {
    }

    /** @return int[] */
    public function getStoriesByCategory(string $category): array
    {
        $response = $this->hackerNewsClient
            ->request(
                'GET',
                sprintf('/v0/%s.json', $category)
            );

        if (200 !== $response->getStatusCode()) {
            throw new HackerNewsApiException($response->getContent(false));
        }

        return $response->toArray();
    }

    public function getStoryById(int $id): ?Story
    {
        $response = $this->hackerNewsClient
            ->request(
                'GET',
                sprintf('/v0/item/%s.json', $id)
            );

        if (200 !== $response->getStatusCode()) {
            throw new HackerNewsApiException($response->getContent(false));
        }

        if ('null' === $response->getContent()) {
            return null;
        }

        return $this->serializer->deserialize(
            $response->getContent(),
            Story::class,
            'json',
        );
    }
}
