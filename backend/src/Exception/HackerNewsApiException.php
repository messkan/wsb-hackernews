<?php

namespace App\Exception;

class HackerNewsApiException extends \RuntimeException
{
    public function __construct($message = '', $code = 503, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
