<?php

declare(strict_types=1);

namespace App\Tests\Resolver;

use App\Resolver\StoryResolver;
use App\Serializable\Story;
use App\Service\HackerNewsService;
use PHPUnit\Framework\TestCase;

class StoryResolverTest extends TestCase
{
    public function testPaginateByCategory(): void
    {
        $hackerNewsService = $this->createMock(HackerNewsService::class);
        $hackerNewsService->expects($this->once())
            ->method('getStoriesByCategory')
            ->with('topstories')
            ->willReturn([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

        $storyResolver = new StoryResolver($hackerNewsService);
        $result = $storyResolver->paginateByCategory('topstories', 5, 1);

        $this->assertSame([6, 7, 8, 9, 10], $result);
    }

    public function testGet(): void
    {
        $hackerNewsService = $this->createMock(HackerNewsService::class);
        $hackerNewsService->expects($this->once())
            ->method('getStoryById')
            ->with(123)
            ->willReturn(
                (new Story())
                ->setId(123)
                ->setTitle('Test Story')
                ->setUrl('https://example.com')
                ->setBy('author')
                ->setKids([1, 1, 5])
                ->setDescendants(12)
                ->setScore(145)
            );

        $storyResolver = new StoryResolver($hackerNewsService);
        $result = $storyResolver->get(123);

        $this->assertInstanceOf(Story::class, $result);
        $this->assertSame(123, $result->getId());
        $this->assertSame('Test Story', $result->getTitle());
        $this->assertSame('https://example.com', $result->getUrl());
        $this->assertSame('author', $result->getBy());
        $this->assertSame(145, $result->getScore());
        $this->assertSame([1, 1, 5], $result->getKids());
    }
}
