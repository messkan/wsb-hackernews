<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__)
    ->exclude('config')
    ->exclude('var')
    ->exclude('public/bundles')
    ->exclude('public/build')
    ->notPath('bin/console')
    ->notPath('public/index.php');

return (new PhpCsFixer\Config())
    ->setRiskyAllowed(true)
    ->setRules([
        '@Symfony' => true,
        '@PHP80Migration' => true,
        'array_syntax' => ['syntax' => 'short'],
        'array_indentation' => true,
        'multiline_whitespace_before_semicolons' => false,
        'blank_line_after_opening_tag' => true,
         'no_unused_imports' => true,
         'ordered_imports' => true,
         'single_import_per_statement' => true,
         'single_line_after_imports' => true,
        'braces' => [
            'allow_single_line_closure' => true,
        ],
        'no_extra_blank_lines' => [
            'tokens' => [
                'curly_brace_block',
                'extra',
                'parenthesis_brace_block',
                'square_brace_block',
                'throw',
                'use',
            ],
        ],
        'whitespace_after_comma_in_array' => true,
        'ternary_operator_spaces' => true,
        'phpdoc_align' => true,
        'no_empty_comment' => true,
        'no_empty_phpdoc' => true,
        // 'no_empty_statement' => true,
        'single_blank_line_before_namespace' => true,
    ])
    ->setFinder($finder);